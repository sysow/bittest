#include "Soldier.h"
CSoldier::CSoldier(string name, double force, double dexterity, double strength)
{
	this->force = force;
	this->strength = strength;
	this->dexterity = dexterity;
	this->name = name;
	strengthStartValue = strength;
	life = 1;
	status = false;
}
CSoldier::CSoldier(string name, double force, double dexterity, double strength, int life)
	:CSoldier(name, force, dexterity, strength)
{
	this->life = life;
}
CSoldier::CSoldier()
{
	name = "Stalker";
	force = 2.0;
	dexterity = 2.0;
	strength = 2.0;
	life = 0;
	strengthStartValue = strength;
	status = false;
}
CSoldier::CSoldier(const CSoldier & other)
{
	SetLife(other.GetLife());
	SetStatus(other.GetStatus());
	SetStrengthStartValue(other.GetStrengthStartValue());
	SetName(other.GetName());
	SetForce(other.GetForce());
	SetDexterity(other.GetDexterity());
	SetStrength(other.GetStrength());
}
CSoldier::~CSoldier()
{
}
void CSoldier::IntroduceYorself()
{
	cout << "My name is:	" << GetName() << endl;
	cout << "force:	" << GetForce() << endl;
	cout << "strengh:	" << GetStrength() << endl;
	cout << "dex :	" << GetDexterity() << endl;
	cout << "life:	" << GetLife() << endl;
	cout << "my Status:	" << status << endl;
}
void CSoldier::PrintHP(CSoldier other)const
{
	cout << GetName() << " ma: " << GetStrength() << " Wyrzymalosci";
	cout << " Walczy z ";
	cout << other.GetName() << "ma: " << other.GetStrength() << " HP " << endl;
}
void CSoldier::SetDoubleParams()
{
	force *= 2;
	strength *= 2;
	dexterity *= 2;
}
void CSoldier::SubtractStrength(double value)
{
	strength -= value;
}
CSoldier & CSoldier::operator+(CSoldier& other)
{
	this->SetDoubleParams();
	return (*this);
}
CSoldier CSoldier::Fight(CSoldier & enemy)
{
	HANDLE hOut;
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
	while (GetStrength() >= 0 && enemy.GetStrength() >= 0)
	{
		system("pause>NULL");
		//Sleep(500);
		int value = 5;
		double enemyAttack = AttackFactor(value);
		double myAttack = enemy.AttackFactor(value);
		if (enemyAttack >= myAttack)
		{
			//PrintHP(enemy);
			SubtractStrength(enemyAttack);
			cout << GetName() << " otrzymal ";
			SetConsoleTextAttribute(hOut, FOREGROUND_RED | FOREGROUND_INTENSITY);
			cout << enemyAttack << " obrazen !!";
			SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
			cout << " i zostalo mu: ";
			SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			cout << GetStrength() << "HP" << endl;
			SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);

		}
		else
		{
			//PrintHP(enemy);
			enemy.SubtractStrength(myAttack);
			cout << enemy.GetName() << " otrzymal ";
			SetConsoleTextAttribute(hOut, FOREGROUND_RED | FOREGROUND_INTENSITY);
			cout << myAttack << " obrazen !!";
			SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
			cout << " i zostalo mu : ";
			SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			cout << enemy.GetStrength() << "HP" << endl;
			SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);

		}
		cout << " _________________________________________" << endl;
		if (GetStrength() > 0)
		{
			CheckLife();
			enemy.CheckLife();
		}
		else
		{
			CheckLife();
			enemy.CheckLife();
		}
	}
	if (GetStrength() > 0)
	{
		CheckLife();
		enemy.CheckLife();
		cout << "Enemy solder is dead" << endl;
		return (*this);
	}
	else
	{
		CheckLife();
		enemy.CheckLife();
		cout << "Solder is dead" << endl;
		return (enemy);

	}
}

double CSoldier::AttackFactor(int dividedBy)
{
	double	tmpFactor = (((1.5*GetDexterity()) + (1.1*GetStrength()))
		/ (0.9*GetForce()));
	return (tmpFactor + RandomNumber(tmpFactor)) / (dividedBy / 2);
}

void CSoldier::SetLife(int value)
{
	life -= 1;
}
void CSoldier::SetLife(unsigned life)
{
	this->life = life;
}
void CSoldier::SetStrengthStartValue(double strengthStartValue)
{
	this->strengthStartValue = strengthStartValue;
}
void CSoldier::SetName(string name)
{
	this->name = name;
}
void CSoldier::SetForce(double force)
{
	this->force = force;

}
void CSoldier::SetDexterity(double dexterity)
{
	this->dexterity = dexterity;
}
void CSoldier::SetStrength(double strength)
{
	this->strength = strength;
}
void CSoldier::CheckLife()
{
	if (GetLife() >= 1 && GetStrength() <= 0)
	{
		GetStrengthStartValue() < 0 ? SetStrength(-1) : SetStrength(GetStrengthStartValue());
		SetLife(-1);
		if (GetLife() <= 0)
			SetStatus(true);
	}
	else
	{
		if (GetStrength() <= 0 && GetLife() <= 0)
			SetStatus(true);
	}
}
void CSoldier::SetStatus(bool state)
{
	status = state;
}
string CSoldier::GetName()const
{
	return name;
}
double CSoldier::GetForce()const
{
	return force;
}
double CSoldier::GetDexterity()const
{
	return dexterity;
}
double CSoldier::GetStrength()const
{
	return strength;
}
bool CSoldier::GetStatus()const
{
	return status;
}
int CSoldier::GetLife()const
{
	return life;
}
double CSoldier::GetStrengthStartValue()const
{
	return strengthStartValue;
}
ostream & operator<<(ostream & screen, CSoldier & soldier)
{
	screen << soldier.GetName() << endl;
	return screen;
}
