#include <iostream>
#include "Soldier.h"
#include "Army.h"

using namespace std;
double RandomNumber(int range) {

	if (range == 0)
		return 1;
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> dist(1, range);
	double number = dist(gen);
	return number;
}
int main()
{

	CSoldier Jan("Jan", 2, 2, 2, 2);
	CSoldier Stacho("Stacho", 2, 2, 2, 1);
	//	Jan.Fight(Stacho);

	int value = 5;
	CArmy a1;
	CArmy a2;
	for (int i = 0; i < value; i++)
	{
		a1.GenerateSoldierInArmy(value * 2);
		a2.GenerateSoldierInArmy(value * 2);
	}
	a1.Fight(a2);
	system("pause");
	return 0;
}