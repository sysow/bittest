#pragma once
#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>
#include <windows.h>
#include <stdlib.h>
using namespace std;
class CSoldier
{
private:
	bool status; // Czy pokonany domyslnie false
	double strengthStartValue; // warto�� pocz�tkowa r�wna strength przy tworzeniu obiektu
	unsigned life;
	string name;
	double force;
	double dexterity;
	double strength;
public:
	CSoldier();
	CSoldier(const CSoldier & other);
	CSoldier(string name, double force,
		double dexterity, double strength);
	CSoldier(string name, double force,
		double dexterity, double strength, int life);
	~CSoldier();
	CSoldier Fight(CSoldier & other);
	double  AttackFactor(int dividedBy);
	void IntroduceYorself();
	void PrintHP(CSoldier other)const;
	void CheckLife();
	string GetName()const;
	double GetForce()const;
	double GetDexterity()const;
	double GetStrength()const;
	bool GetStatus()const;
	int GetLife()const;
	double GetStrengthStartValue()const;
	void SetDoubleParams();
	void SubtractStrength(double value);
	void SetStatus(bool state);
	void SetLife(int value);
	void SetLife(unsigned life);
	void SetStrengthStartValue(double strengthStartValue);
	void SetName(string name);
	void SetForce(double force);
	void SetDexterity(double dexterity);
	void SetStrength(double strength);
	CSoldier & operator+(CSoldier& other);
	friend  ostream & operator<<(ostream & screen, CSoldier & soldier);
	friend double RandomNumber(int range);

};

