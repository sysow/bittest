#include "Army.h"
void CArmy::RandomizeArmy()
{
	random_shuffle(army.begin(), army.end());
}
void CArmy::AddSoldier()
{
	army.push_back(CSoldier());
}

bool CArmy::CheckArmy()
{
	int tmp = GetArmySize();
	for (int i = 0; i < GetArmySize(); i++)
	{
		if (GetSoldier(i).GetStatus() == true)
			tmp--;
	}
	//cout << "Zostalo " << tmp << " Tylu Zywych zoniezy z " << GetArmySize() << endl;
	for (int i = 0; i < GetArmySize(); i++)
	{
		if (GetSoldier(i).GetStatus() == true)
		{
			Out(i);
			//cout << "Usuwam " << i << "-tego Soldiera" << endl;
		}
		else
		{
			//	cout << "Nie usuwam " << i << "-tego Soldiera" << endl;
		}
	}
	return tmp <= 0 ? false : true;
}

bool CArmy::GetSoldierStatus(int number)
{
	return GetSoldier(number).GetStatus();
}

void CArmy::AddSoldier(CSoldier&  other)
{
	army.push_back(other);
}

void CArmy::GenerateSoldierInArmy(int range)
{
	string name = "Solder ";
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> dist(97, 122);
	int number;
	for (int i = 0; i < 5; i++)
	{
		number = dist(gen);
		name += (char)number;
	}
	army.push_back(CSoldier(name, RandomNumber(range),
		RandomNumber(range), RandomNumber(range), range / 2));
}

void CArmy::Out(int number)
{
	if (number < GetArmySize())
	{
		army.erase(army.begin() + number);
	}
	else
	{
		cout << "Jestes poza vectorem ! Out!!!" << endl;
	}
}




bool CArmy::GetIsDefeated()const
{
	return isDefeated;
}

CSoldier & CArmy::GetSoldier(int number)
{
	return army[number];
}
int CArmy::GetArmySize()const
{
	return army.size();
}


CArmy::CArmy()
{
	isDefeated = false;
	//army.push_back(CSoldier());
}

void CArmy::Fight(CArmy & enemy)
{

	while (GetArmySize() >= 1 && enemy.GetArmySize() >= 1)
	{
		for (int i = 0; i < GetArmySize(); i++)
		{
			int liczba = RandomNumber(enemy.GetArmySize());
			if (liczba == enemy.GetArmySize())
				liczba--;
			if (enemy.GetArmySize() == 0)
				break;
			//cout << " Wylosowan liczba randomowa to: " << liczba << endl;
			//cout << " A rozmiar wrogiej armi to  " << enemy.GetArmySize() << endl;
			GetSoldier(i).Fight(enemy.GetSoldier(liczba));
			//cout << "Pierwsza Armia" << endl;
			CheckArmy();
			//cout << "Pierwsza Armia" << endl;

			enemy.CheckArmy();
		}
	}
}

CArmy::CArmy(CSoldier & other)
{
	isDefeated = false;
	army.push_back(other);
}
CArmy::~CArmy()
{
}
ostream & operator<<(ostream & screen, CArmy & army)
{
	for (int i = 0; i < army.GetArmySize(); i++)
		screen << army.GetSoldier(i);
	return screen;
}

