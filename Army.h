#pragma once
#include "Soldier.h"
#include <vector>
#include <algorithm> 
#include <typeinfo>
#include <random>
#include <windows.h>
#include <stdlib.h>
class CArmy
{
private:
	vector<CSoldier>army;
	bool isDefeated;
public:
	CArmy(CSoldier& other);
	CArmy();
	void Fight(CArmy & enemy);
	int GetArmySize()const;
	bool GetIsDefeated()const;
	void RandomizeArmy();
	void AddSoldier();
	bool CheckArmy();
	bool GetSoldierStatus(int number);
	void AddSoldier(CSoldier& other);
	void GenerateSoldierInArmy(int range);
	void Out(int number);
	CSoldier & GetSoldier(int number);
	friend double RandomNumber(int range);
	friend ostream & operator<<(ostream & screen, CArmy & army);
	~CArmy();
};

